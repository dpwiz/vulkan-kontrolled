module Render.Swapchain where

import App.Prelude

import Geomancy.Vulkan.Projection (orthoOffCenter)
import Geomancy.Vulkan.View (orthoFitScreen)
import Vulkan.Setup.Swapchain (Swapchain)

import qualified Data.Vector as Vector
import qualified Data.Vector.Storable as Storable
import qualified Linear
import qualified Vulkan.Core10 as Vk
import qualified Vulkan.Setup.Buffer as Buffer
import qualified Vulkan.Setup.Context as Context
import qualified Vulkan.Setup.Image as Image
import qualified Vulkan.Setup.Render.Forward as Forward
import qualified Vulkan.Setup.Render.Pipeline as Pipeline
import qualified Vulkan.Setup.Render.Texture as Texture
import qualified Vulkan.Setup.Samplers as Samplers
import qualified Vulkan.Setup.Swapchain as Swapchain

import qualified Render.Pipeline.Font as Font
import qualified Render.Pipeline.Line as Line
import qualified Render.Pipeline.Shared as Shared
import qualified Render.Pipeline.Solid as Solid

import qualified Render.Types as Render

import Game.Setup.Env (Env(..))

-- * Swapchain image resources

createForSwapchain :: Env -> Swapchain Vk.ImageView -> RIO App Render.ForSwapchain
createForSwapchain env swapchain = do
  context <- asks appContext
  let vkLogical = Context.getVkLogical context

  forward <- Forward.createMsaa context swapchain
  let forwardPass = Forward.fmRenderPass forward

  samplers <- Samplers.create8 context
  let set0dslb = Shared.set0 samplers (envTextures env) mempty

  pipelines <- Render.Pipelines
    <$> Font.create  extent forwardPass set0dslb
    <*> Line.create  extent forwardPass set0dslb
    <*> Solid.create extent forwardPass set0dslb

  -- XXX: use this pipeline as a refernce for a shared layout
  let set0layout = Vector.take 1 $ Pipeline.pDescLayouts (Render.solid pipelines)

  descPool <- Vk.createDescriptorPool vkLogical dpCI Nothing
  set0 <- fmap Vector.head $ Vk.allocateDescriptorSets vkLogical zero
    { Vk.descriptorPool = descPool
    , Vk.setLayouts = set0layout
    }

  withMVar (envSceneData env) \old@Buffer.Allocated{aBuffer} -> do
    let
      Vk.Extent2D{width, height} = extent
      ortho = orthoOffCenter 0 1 width height
      scale = orthoFitScreen (fromIntegral width) (fromIntegral height) sWidth sHeight
        where
          Linear.V2 sWidth sHeight = size

      new = Storable.singleton Render.Scene
        { sceneProjection    = ortho
        , sceneInvProjection = mempty
        , sceneView          = scale
        , sceneInvView       = mempty
        , sceneUi            = ortho <> scale
        }
    _same <- Buffer.updateCoherent new old

    let
      writeSet0b0 = SomeStruct zero
        { Vk.dstSet          = set0
        , Vk.dstBinding      = 0
        , Vk.dstArrayElement = 0
        , Vk.descriptorCount = 1
        , Vk.descriptorType  = Vk.DESCRIPTOR_TYPE_UNIFORM_BUFFER
        , Vk.bufferInfo      = Vector.singleton set0bind0
        }
        where
          set0bind0 = Vk.DescriptorBufferInfo
            { Vk.buffer = aBuffer
            , Vk.offset = 0
            , Vk.range  = Vk.WHOLE_SIZE
            }

      writeSet0b2 = SomeStruct zero
        { Vk.dstSet          = set0
        , Vk.dstBinding      = 2
        , Vk.dstArrayElement = 0
        , Vk.descriptorType  = Vk.DESCRIPTOR_TYPE_SAMPLED_IMAGE
        , Vk.descriptorCount = fromIntegral $ Vector.length (envTextures env)
        , Vk.imageInfo       = textureInfos
        }
        where
          textureInfos = do
            texture <- envTextures env
            pure Vk.DescriptorImageInfo
              { sampler     = zero
              , imageView   = Image.aiImageView $ Texture.tAllocatedImage texture
              , imageLayout = Vk.IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL
              }

      -- writeSet0b3 = SomeStruct zero
      --   { Vk.dstSet          = set0
      --   , Vk.dstBinding      = 3
      --   , Vk.dstArrayElement = 0
      --   , Vk.descriptorType  = Vk.DESCRIPTOR_TYPE_SAMPLED_IMAGE
      --   , Vk.descriptorCount = fromIntegral $ Vector.length (envCubes env)
      --   , Vk.imageInfo       = cubeInfos
      --   }
      --   where
      --     cubeInfos = do
      --       texture <- envCubes env
      --       pure Vk.DescriptorImageInfo
      --         { sampler     = zero
      --         , imageView   = Image.aiImageView $ Texture.tAllocatedImage texture
      --         , imageLayout = Vk.IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL
      --         }

      writeSets = Vector.fromList
        [ writeSet0b0
        , writeSet0b2
        -- , writeSet0b3
        ]
    Vk.updateDescriptorSets vkLogical writeSets mempty

  pure Render.ForSwapchain
    { fsSwapchain = swapchain
    -- , fsShadow = shadow
    , fsRender    = forward
    , fsDescPool  = descPool
    -- , fsShadowDescPool  = descPool
    , fsSamplers  = samplers
    , fsPipelines = pipelines
    , fsSceneDesc = set0
    }
  where
    extent = Swapchain.extent swapchain
    -- swapLength = fromIntegral . Vector.length $ Swapchain.views swapchain

    dpCI = descPoolCI MAX_SETS
      [ ( Vk.DESCRIPTOR_TYPE_UNIFORM_BUFFER
        , uniformBuffers
        )
      , ( Vk.DESCRIPTOR_TYPE_SAMPLED_IMAGE
        , sampledImages
        )
      , ( Vk.DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER
        , sampledImages * numPipelines
        )
      ]
      where
        numPipelines = 3
        uniformBuffers = 1 -- 1 shared camera
        sampledImages = fromIntegral $
          Vector.length (envTextures env)

pattern MAX_SETS :: (Eq a, Num a) => a
pattern MAX_SETS = 1

pattern INFLIGHT_FRAMES :: (Eq a, Num a) => a
pattern INFLIGHT_FRAMES = 2

descPoolCI
  :: Word32
  -> [(Vk.DescriptorType, Word32)]
  -> Vk.DescriptorPoolCreateInfo '[]
descPoolCI maxSets sizes = zero
  { Vk.maxSets   = maxSets
  , Vk.poolSizes = Vector.fromList $ map (uncurry Vk.DescriptorPoolSize) sizes
  }

destroyForSwapchain :: Render.ForSwapchain -> RIO App ()
destroyForSwapchain Render.ForSwapchain{..} = do
  context <- asks appContext

  traverse_ (Samplers.destroy context) fsSamplers

  Vk.destroyDescriptorPool (Context.getVkLogical context) fsDescPool Nothing

  Pipeline.destroy context $ Render.font fsPipelines
  Pipeline.destroy context $ Render.line fsPipelines
  Pipeline.destroy context $ Render.solid fsPipelines

  Forward.destroyMsaa context fsRender

-- -- | Correct for screen aspect ratio so the squares will be drawn square.
-- adjustScreen :: Vk.Extent2D -> Mat4
-- adjustScreen extent =
--   mat4
--     x 0 0 0
--     0 y 0 0
--     0 0 z 0
--     0 0 0 1
--   where
--     x = s / width
--     y = s / height
--     z = 1.0

--     s = 2 * min (width / sx) (height / sy)
--       where
--         Linear.V2 sx sy = size

--     (width, height) = (fromIntegral iWidth, fromIntegral iHeight)
--       where
--         Vk.Extent2D{width=iWidth, height=iHeight} = extent

size :: Num a => Linear.V2 a
size = Linear.V2 220 360
