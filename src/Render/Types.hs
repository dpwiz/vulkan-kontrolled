module Render.Types where

import App.Prelude

import Vulkan.Setup.Render.Forward (ForwardMsaa)
import Vulkan.Setup.Render.Pipeline (Pipeline)
import Vulkan.Setup.Swapchain (Swapchain)

import qualified Foreign
import qualified Vulkan.Core10 as Vk

type Inflights = MVar ("primary" ::: Inflight, "secondary" ::: Inflight)

data Inflight = Inflight
  { _iReady          :: Vk.Fence -- XXX: signalled = inflight frame available for building, reset = busy
  , _iSubmitted      :: Vk.Fence
  , _iImageAvailable :: Vk.Semaphore
  , _iRenderFinished :: Vk.Semaphore -- XXX: actually, "command buffer submitted"
  , _iCommandPool    :: Vk.CommandPool
  }

data ForSwapchain = ForSwapchain
  { fsSwapchain  :: Swapchain Vk.ImageView
  -- , fsShadow     :: Shadow
  , fsRender     :: ForwardMsaa
  , fsDescPool   :: Vk.DescriptorPool
  , fsSamplers   :: Vector Vk.Sampler
  , fsPipelines  :: Pipelines
  , fsSceneDesc  :: Vk.DescriptorSet
  }

data Pipelines = Pipelines
  { font     :: Pipeline
  , line     :: Pipeline
  , solid    :: Pipeline
  }

data Scene = Scene
  { sceneProjection    :: Mat4
  , sceneInvProjection :: Mat4
  , sceneView          :: Mat4
  , sceneInvView       :: Mat4
  , sceneUi            :: Mat4
  }

instance Storable Scene where
  alignment ~_ = 16

  sizeOf ~_ = 64 * 5

  peek ptr = do
    sceneProjection    <- Foreign.peek        (Foreign.castPtr ptr)
    sceneInvProjection <- Foreign.peekElemOff (Foreign.castPtr ptr) 1
    sceneView          <- Foreign.peekElemOff (Foreign.castPtr ptr) 2
    sceneInvView       <- Foreign.peekElemOff (Foreign.castPtr ptr) 3
    sceneUi            <- Foreign.peekElemOff (Foreign.castPtr ptr) 4
    pure Scene{..}

  poke ptr Scene{..} = do
    Foreign.poke        (Foreign.castPtr ptr)   sceneProjection
    Foreign.pokeElemOff (Foreign.castPtr ptr) 1 sceneInvProjection
    Foreign.pokeElemOff (Foreign.castPtr ptr) 2 sceneView
    Foreign.pokeElemOff (Foreign.castPtr ptr) 3 sceneInvView
    Foreign.pokeElemOff (Foreign.castPtr ptr) 4 sceneUi
