module Render.Pipeline.Shared where

import App.Prelude

import Vulkan.Setup.Render.Texture (Texture, CubeMap, Flat)

import qualified Data.Vector as Vector
import qualified Vulkan.Core10 as Vk

-- * Common types

-- ** Packed floats triple

type Pos = Packed Vec3

newtype Packed a = Packed a
  deriving (Eq, Show)

instance Storable (Packed Vec3) where
  sizeOf ~_ = 12

  alignment ~_ = 4

  peek ptr = fmap Packed $ peek (castPtr ptr)

  poke ptr (Packed v3) = poke (castPtr ptr) v3

-- * Common descriptor set

set0
  :: Vector Vk.Sampler
  -> Vector (Texture Flat)
  -> Vector (Texture CubeMap)
  -> [Vk.DescriptorSetLayoutBinding]
set0 samplers textures cubes =
  [ set0bind0
  , set0bind1 samplers
  , set0bind2 textures
  -- , set0bind3 cubes
  ]

set0bind0 :: Vk.DescriptorSetLayoutBinding
set0bind0 = Vk.DescriptorSetLayoutBinding
  { binding           = 0
  , descriptorType    = Vk.DESCRIPTOR_TYPE_UNIFORM_BUFFER
  , descriptorCount   = 1
  , stageFlags        = Vk.SHADER_STAGE_ALL
  , immutableSamplers = mempty
  }

set0bind1 :: Vector Vk.Sampler -> Vk.DescriptorSetLayoutBinding
set0bind1 samplers = Vk.DescriptorSetLayoutBinding
  { Vk.binding           = 1
  , Vk.stageFlags        = Vk.SHADER_STAGE_FRAGMENT_BIT
  , Vk.descriptorType    = Vk.DESCRIPTOR_TYPE_SAMPLER
  , Vk.descriptorCount   = fromIntegral $ Vector.length samplers
  , Vk.immutableSamplers = samplers
  }

set0bind2 :: Vector (Texture Flat) -> Vk.DescriptorSetLayoutBinding
set0bind2 textures = Vk.DescriptorSetLayoutBinding
  { binding           = 2
  , descriptorType    = Vk.DESCRIPTOR_TYPE_SAMPLED_IMAGE
  , descriptorCount   = fromIntegral $ Vector.length textures
  , stageFlags        = Vk.SHADER_STAGE_FRAGMENT_BIT
  , immutableSamplers = mempty
  }

set0bind3 :: Vector (Texture CubeMap) -> Vk.DescriptorSetLayoutBinding
set0bind3 cubes = Vk.DescriptorSetLayoutBinding
  { binding           = 3
  , descriptorType    = Vk.DESCRIPTOR_TYPE_SAMPLED_IMAGE
  , descriptorCount   = fromIntegral $ Vector.length cubes
  , stageFlags        = Vk.SHADER_STAGE_FRAGMENT_BIT
  , immutableSamplers = mempty
  }

vertexPos :: (Vk.VertexInputRate, [Vk.Format])
vertexPos =
  ( Vk.VERTEX_INPUT_RATE_VERTEX
  , [Vk.FORMAT_R32G32B32_SFLOAT]
  )

-- module Render.Pipeline.Shared where

-- import Prelude

-- import qualified Vulkan.Core10 as Vk

-- set0 :: [Vk.DescriptorSetLayoutBinding]
-- set0 =
--   [ set0bind0
--   ]

-- set0bind0 :: Vk.DescriptorSetLayoutBinding
-- set0bind0 = Vk.DescriptorSetLayoutBinding
--   { binding           = 0
--   , descriptorType    = Vk.DESCRIPTOR_TYPE_UNIFORM_BUFFER
--   , descriptorCount   = 1
--   , stageFlags        = Vk.SHADER_STAGE_ALL
--   , immutableSamplers = mempty
--   }

-- vertexPos :: (Vk.VertexInputRate, [Vk.Format])
-- vertexPos =
--   ( Vk.VERTEX_INPUT_RATE_VERTEX
--   , [Vk.FORMAT_R32G32B32_SFLOAT]
--   )
