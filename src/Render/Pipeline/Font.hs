module Render.Pipeline.Font where

import RIO

import Vulkan.Zero (zero)
import Vulkan.Setup.Render.Pipeline (Pipeline)
import Vulkan.Utils.ShaderQQ (vert, frag)

import qualified Data.Vector.Storable as Storable
import qualified Vulkan.Core10 as Vk
import qualified Vulkan.Setup.Buffer as Buffer
import qualified Vulkan.Setup.Render.Pipeline as Pipeline
import qualified Vulkan.Setup.Resource.Font as Font

import App.Types (App(..))

type CoherentInstances = Buffer.Allocated 'Buffer.Coherent Font.PutChar

create :: Vk.Extent2D -> Vk.RenderPass -> [Vk.DescriptorSetLayoutBinding] -> RIO App Pipeline
create extent renderPass set0 = do
  context <- asks appContext

  Pipeline.create context extent renderPass zero
    { Pipeline.cVertexCode   = Just vertCode
    , Pipeline.cFragmentCode = Just fragCode
    , Pipeline.cDescLayouts  = [set0]
    , Pipeline.cVertexInput  = vertexInput
    , Pipeline.cDepthTest    = False
    , Pipeline.cDepthWrite   = False
    , Pipeline.cBlend        = True
    , Pipeline.cCull         = Vk.CULL_MODE_NONE
    }
  where
    vertexInput = Pipeline.vertexInput
      [ (Vk.VERTEX_INPUT_RATE_INSTANCE, instanceAttrs)
      ]

    instanceAttrs =
      [ Vk.FORMAT_R32G32_SFLOAT -- Quad position
      , Vk.FORMAT_R32G32_SFLOAT -- Quad size
      , Vk.FORMAT_R32G32_SFLOAT -- UV Offset
      , Vk.FORMAT_R32G32_SFLOAT -- UV Scale
      ]

vertCode :: ByteString
vertCode =
  [vert|
    #version 450
    #extension GL_ARB_separate_shader_objects : enable

    layout(set=0, binding=0) uniform Globals {
      mat4 projection;
      mat4 invProjection;
      mat4 view;
      mat4 invView;
      mat4 ui;
    } scene;

    layout(location = 0) in vec2 iPosition;
    layout(location = 1) in vec2 iSize;
    layout(location = 2) in vec2 iOffset;
    layout(location = 3) in vec2 iScale;

    layout(location = 0) out vec2 fTexCoord;

    vec2 positions[6] = vec2[](
      vec2(-0.5, 0.5),
      vec2(0.5, 0.5),
      vec2(-0.5, -0.5),

      vec2(0.5, -0.5),
      vec2(-0.5, -0.5),
      vec2(0.5, 0.5)
    );

    vec2 texCoords[6] = vec2[](
      vec2(0.0, 0.0),
      vec2(1.0, 0.0),
      vec2(0.0, 1.0),

      vec2(1.0, 1.0),
      vec2(0.0, 1.0),
      vec2(1.0, 0.0)
    );

    void main() {
      vec2 pos = positions[gl_VertexIndex];
      gl_Position = scene.ui * vec4(pos * iSize + iPosition, 0, 1.0);
      gl_Position.z = 0.001;

      vec2 uv = texCoords[gl_VertexIndex];
      fTexCoord = uv * iScale + iOffset;
    }
  |]

fragCode :: ByteString
fragCode =
  [frag|
    #version 450
    #extension GL_ARB_separate_shader_objects : enable

    layout(set=0, binding=1) uniform sampler samplers[8];
    layout(set=0, binding=2) uniform texture2D textures[1]; // MAX_TEXTURES

    layout(location = 0) in vec2 fTexCoord;

    layout(location = 0) out vec4 outColor;

    const float smoothing = 1.0/16.0;
    const float outlineWidth = 3.0/16.0;
    const float outerEdgeCenter = 0.5 - outlineWidth;

    const vec3 color = vec3(1);
    const vec3 outlineColor = vec3(0);

    void main() {
      float sdf = texture(
        sampler2D(
          textures[0],
          samplers[0]
        ),
        fTexCoord
      ).r;

      float alpha = smoothstep(outerEdgeCenter - smoothing, outerEdgeCenter + smoothing, sdf);
      float border = smoothstep(0.5 - smoothing, 0.5 + smoothing, sdf);
      outColor = vec4(mix(outlineColor, color, border), alpha);
    }
  |]

createInstances
  :: [Font.PutChar]
  -> RIO App (Buffer.Allocated 'Buffer.Coherent Font.PutChar)
createInstances is = do
  ctx <- asks appContext
  Buffer.createCoherent ctx Vk.BUFFER_USAGE_VERTEX_BUFFER_BIT 0 (Storable.fromList is)
