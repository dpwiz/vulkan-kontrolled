module Game.Setup.Env
  ( Env(..)
  , withEnv
  , createEnv
  , destroyEnv
  ) where

import App.Prelude

import Vulkan.Setup.Render.Texture (Texture, {- CubeMap, -} Flat)
import Vulkan.Setup.Resource.KTX (createKtx1)
import Control.Concurrent (forkIO)

import qualified Apecs
import qualified Data.Vector as Vector
import qualified Data.Vector.Storable as Storable
import qualified Vulkan.Core10 as Vk
import qualified Vulkan.Setup.Buffer as Buffer
import qualified Vulkan.Setup.Render.Model as Model
import qualified Vulkan.Setup.Render.Texture as Texture
import qualified Sound.ControlHold.Client as ControlHold

import Game.World (World)

import qualified Game.Setup.World as World
import qualified Game.Events.Kontrol as Kontrol
import qualified Render.Inflight as Inflight
import qualified Render.Pipeline.Line as Line
import qualified Render.Pipeline.Solid as Solid
import qualified Render.Types as Render

-- TODO: repack App?
data Env = Env
  { envInflights      :: Render.Inflights
  , envSwapchain      :: TMVar Render.ForSwapchain

  , envSceneData       :: MVar (Buffer.Allocated 'Buffer.Coherent Render.Scene)

  , envWorld          :: World
  , envKontrol        :: ThreadId

    -- Resources
  , envTextures :: Vector (Texture Flat)
  -- , envCubes    :: Vector (Texture CubeMap)

  , envPlayerModel    :: Solid.Staged
  , envPlayerInstance :: MVar Solid.CoherentInstances

  , envTargetModel     :: Solid.Staged
  , envTargetInstances :: MVar Solid.CoherentInstances

  , envBulletModel     :: Solid.Staged
  , envBulletInstances :: MVar Solid.CoherentInstances

  , envParticles       :: MVar Line.Coherent
  }

withEnv :: (Env -> RIO App c) -> RIO App c
withEnv = bracket createEnv destroyEnv

createEnv :: RIO App Env
createEnv = do
  ctx <- asks appContext
  logInfo "Creating app environment"

  envInflights <- Inflight.create
  envSwapchain <- newEmptyTMVarIO

  scene <- Buffer.createCoherent ctx Vk.BUFFER_USAGE_UNIFORM_BUFFER_BIT 1 $
    Storable.singleton Render.Scene
      { sceneProjection    = mempty
      , sceneInvProjection = mempty
      , sceneView          = mempty
      , sceneInvView       = mempty
      , sceneUi            = mempty
      }
  envSceneData <- newMVar scene

  font <- asks appFont
  envWorld <- World.init ctx font
  -- handler <- toIO $ kontroller envWorld
  envKontrol <- liftIO . forkIO $ ControlHold.runDefault (Kontrol.handler envWorld)

  -- XXX: can such use of inflight command pool cause memory hazard?
  withMVar envInflights \(Render.Inflight{_iCommandPool=cmd}, _next) -> do
    envTextures <- traverse (liftIO . createKtx1 ctx cmd) $ Vector.fromList
      [ "resources" </> "font.ktx"
      ]

    envPlayerModel <- Model.createStaged ctx cmd Solid.playerVertices (Just Solid.triIndices)
    envPlayerInstance <- Solid.createInstance 0 0 0 >>= newMVar

    envTargetModel <- Model.createStaged ctx cmd Solid.targetVertices (Just Solid.quadIndices)
    envTargetInstances <- Solid.createInstance 0 0 0 >>= newMVar

    envBulletModel <- Model.createStaged ctx cmd Solid.bulletVertices (Just Solid.quadIndices)
    envBulletInstances <- Solid.createInstance 0 0 0 >>= newMVar

    envParticles <- Model.createCoherent ctx 1 >>= newMVar

    pure Env{..}

destroyEnv :: Env -> RIO App ()
destroyEnv Env{..} = do
  ctx <- asks appContext
  logInfo "Destroying app environment"

  Apecs.runWith envWorld do
    Apecs.cmapM_ \World.TextLine{textInstances} ->
      Buffer.destroyAll ctx [textInstances]

  -- traverse_ (Texture.destroy ctx) envCubes
  traverse_ (Texture.destroy ctx) envTextures

  tryReadMVar envSceneData >>=
    Buffer.destroyAll ctx

  player <- takeMVar envPlayerInstance
  targets <- takeMVar envTargetInstances
  bullets <- takeMVar envBulletInstances
  Buffer.destroyAll ctx [player, targets, bullets]

  takeMVar envParticles >>= Model.destroyIndexed ctx

  traverse_ (Model.destroyIndexed ctx)
    [ envPlayerModel
    , envTargetModel
    , envBulletModel
    ]

  Inflight.destroy envInflights
