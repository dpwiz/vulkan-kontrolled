{-# LANGUAGE BangPatterns #-}
{-# LANGUAGE NoImplicitPrelude #-}

module Game.Setup.World
  ( init
  , setup
  , module Game.World
  ) where

import App.Prelude

import qualified Apecs
import qualified Linear
import qualified Vulkan.Setup.Resource.Font as Font

import Game.World
import Game.Events.System (createTextLine, xmax, ymin)

init :: AppContext -> Font.Container -> RIO App World
init ctx font = do
  world <- liftIO initWorld
  runWorld world $ setup ctx font
  pure world

setup :: AppContext -> Font.Container -> SystemW ()
setup ctx font = do
  _window <- Apecs.newEntity Window
    { windowContext = ctx
    , windowWidth  = xmax
    , windowHeight = ymin
    , windowFont   = font
    }

  bb <- createTextLine
    16
    (xmax * 2)
    (ymin * 1.25)
    (Font.Begin, Font.Begin)
    "B / B"
  _bb <- Apecs.newEntity bb

  mb <- createTextLine
    16
    (xmax * 2)
    (ymin * 1.25)
    (Font.Middle, Font.Begin)
    "M / B"
  _mb <- Apecs.newEntity mb

  eb <- createTextLine
    16
    (xmax * 2)
    (ymin * 1.25)
    (Font.End, Font.Begin)
    "E / B"
  _eb <- Apecs.newEntity eb

  bm <- createTextLine
    16
    (xmax * 2)
    (ymin * 1.25)
    (Font.Begin, Font.Middle)
    "B / M"
  _bm <- Apecs.newEntity bm

  mm <- createTextLine
    16
    (xmax * 2)
    (ymin * 1.25)
    (Font.Middle, Font.Middle)
    "M / M"
  _mm <- Apecs.newEntity mm

  em <- createTextLine
    16
    (xmax * 2)
    (ymin * 1.25)
    (Font.End, Font.Middle)
    "E / M"
  _em <- Apecs.newEntity em

  be <- createTextLine
    16
    (xmax * 2)
    (ymin * 1.25)
    (Font.Begin, Font.End)
    "B / E"
  _be <- Apecs.newEntity be

  me <- createTextLine
    16
    (xmax * 2)
    (ymin * 1.25)
    (Font.Middle, Font.End)
    "M / E"
  _me <- Apecs.newEntity me

  score <- createTextLine
    16
    (xmax * 2)
    (ymin * 1.25)
    (Font.End, Font.End)
    "Score: 0"

  _player <- Apecs.newEntity
    ( Player
    , Position $ Linear.V2 0 120
    , Velocity 0
    , score
    )
  -- TODO: move retained data to World

  _row1 <- Apecs.newEntity TargetRow
    { targetRow      = 1
    , targetRate     = 0.6
    , targetPhase    = 0.0
    , targetPosition = Position $ Linear.V2 (-110) (80 - 250)
    , targetVelocity = Velocity $ Linear.V2 80 0
    }

  _row2 <- Apecs.newEntity TargetRow
    { targetRow      = 2
    , targetRate     = 0.6
    , targetPhase    = 0.3
    , targetPosition = Position $ Linear.V2 110 (120 - 250)
    , targetVelocity = Velocity $ Linear.V2 (-80) 0
    }

  pure ()
