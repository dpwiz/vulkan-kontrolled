{-# LANGUAGE TemplateHaskell #-}

module Game.World where

import Apecs
import Apecs.TH (makeMapComponents)
import Control.Monad.IO.Class (MonadIO)
import Linear (V2)
import RIO (Text)

import App.Types (AppContext)

import qualified Render.Pipeline.Font as Font
import qualified Vulkan.Setup.Resource.Font as Font

newtype Position = Position (V2 Float)
  deriving Show

newtype Velocity = Velocity (V2 Float)
  deriving Show

data TargetRow = TargetRow
  { targetRow      :: Int
  , targetRate     :: Float
  , targetPhase    :: Float
  , targetPosition :: Position
  , targetVelocity :: Velocity
  }
  deriving (Show)

instance Component TargetRow where
  type Storage TargetRow = Cache 3 (Map TargetRow)

data Target = Target
  deriving Show

data Shoot = Shoot { shootPeriod :: Float }
  deriving Show

data Bullet = Bullet
  deriving Show

data Particle = Particle Float
  deriving Show

data Player = Player
  deriving Show

instance Component Player where
  type Storage Player = Unique Player

newtype Score = Score Int
  deriving (Show, Num)

instance Semigroup Score where
  (<>) = (+)

instance Monoid Score where
  mempty = 0

instance Component Score where
  type Storage Score = Global Score

newtype Time = Time Float
  deriving (Show, Num)

instance Semigroup Time where
  (<>) = (+)

instance Monoid Time where
  mempty = 0

instance Component Time where
  type Storage Time = Global Time

data Window = Window
  { windowContext :: AppContext
  , windowWidth   :: Float
  , windowHeight  :: Float
  , windowFont    :: Font.Container
  }

instance Component Window where
  type Storage Window = Unique Window

data TextLine = TextLine
  { textLine      :: Text
  , textInstances :: Font.CoherentInstances
  , textSize      :: Float
  , textWidth     :: Float
  , textHeight    :: Float
  , textOrigin    :: (Font.Origin, Font.Origin)
  }
  deriving (Show)

makeMapComponents
  [ ''Position
  , ''Velocity

    , ''Target

  , ''Shoot
  , ''Bullet

  , ''Particle

  , ''TextLine
  ]

makeWorld "World"
  [ ''Position
  , ''Velocity


  , ''TargetRow
  , ''Target

  , ''Shoot
  , ''Bullet

  , ''Player
  , ''Score
  , ''Time
  , ''Particle

  , ''Window
  , ''TextLine
  ]

type Kinetic = (Position, Velocity)

type SystemW a = System World a

{-# INLINE runWorld #-}
runWorld :: MonadIO m => World -> SystemT World IO a -> m a
runWorld world = liftIO . runWith world
