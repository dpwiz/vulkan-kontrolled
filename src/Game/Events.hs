module Game.Events where

import App.Prelude

import Linear (V2(..))

import qualified Apecs
import qualified SDL

import Game.Setup.Env (Env(..))
import Game.Render (drawFrame)
import Game.World

import qualified Game.Events.Time as Time
import qualified Game.Events.System as System

handleEvent
  :: Env
  -> (Bool -> RIO App ())
  -> SDL.Event
  -> RIO App ()
handleEvent Env{envWorld} setQuit SDL.Event{eventPayload} =
  case eventPayload of
    SDL.QuitEvent ->
      setQuit True

    SDL.WindowResizedEvent{} ->
      setQuit False

    SDL.KeyboardEvent SDL.KeyboardEventData{..} ->
      case SDL.keysymKeycode keyboardEventKeysym of
        SDL.KeycodeEscape ->
          setQuit True

        SDL.KeycodeLeft ->
          runWorld envWorld $
            Apecs.cmap \(Player, Velocity _vel) ->
              if pressed then
                Velocity (V2 (-System.playerSpeed) 0)
              else
                Velocity 0

        SDL.KeycodeRight ->
          runWorld envWorld $
            Apecs.cmap \(Player, Velocity _vel) ->
              if pressed then
                Velocity (V2 System.playerSpeed 0)
              else
                Velocity 0

        SDL.KeycodeSpace ->
          runWorld envWorld $
            Apecs.cmap \Player ->
              if pressed then
                Right (Shoot 0.5)
              else
                Left (Apecs.Not @Shoot)
        _ ->
          pure ()
      where
        pressed =
          keyboardEventKeyMotion == SDL.Pressed

    _rest ->
      pure ()

afterEvents
  :: Env
  -> (Bool -> RIO App ())
  -> RIO App ()
afterEvents env setQuit = do
  Time.run env
  drawFrame env (setQuit False)
