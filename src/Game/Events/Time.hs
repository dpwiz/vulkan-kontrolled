module Game.Events.Time where

import App.Prelude

import qualified Apecs
import qualified Data.Vector.Storable as Vector
import qualified Vulkan.Setup.Render.Model as Model
import qualified Vulkan.Setup.Buffer as Buffer

import Game.Setup.Env (Env(..))
import Game.World

import qualified Game.Events.System as System
import qualified Render.Pipeline.Solid as Solid

run :: Env -> RIO App ()
run Env{..} = do
  ctx <- asks appContext

  runWorld envWorld do
    System.step

    Apecs.cmapM_ \(Player, Position pos) ->
      liftIO . modifyMVarMasked_ envPlayerInstance $
        Buffer.updateCoherent $
          Vector.singleton . Solid.Instance $
            System.trs pos 0 20

    bullets <- flip Apecs.cfold mempty \acc (Bullet, Position pos) ->
      Solid.Instance (System.trs pos 0 4) : acc
    liftIO . modifyMVarMasked_ envBulletInstances $
      Buffer.updateCoherentResize_ ctx $
        Vector.fromList bullets

    targets <- flip Apecs.cfold mempty \acc (Target, Position pos) ->
      Solid.Instance (System.trs pos 0 10) : acc
    liftIO . modifyMVarMasked_ envTargetInstances $
      Buffer.updateCoherentResize_ ctx $
        Vector.fromList targets

    particles <- flip Apecs.cfold mempty
      \acc (Particle fuse, vel, pos) ->
        System.particle fuse vel pos : acc
    liftIO . modifyMVarMasked_ envParticles $
      Model.updateCoherent ctx $ mconcat particles
