module Game.Events.System where

import App.Prelude

import System.Random (randomRIO)
import Linear (V2(..))

import qualified Apecs
import qualified Data.Vector.Storable as Storable
import qualified Linear
import qualified RIO.Text as Text
import qualified Vulkan.Core10 as Vk
import qualified Vulkan.Setup.Buffer as Buffer
import qualified Vulkan.Setup.Render.Model as Model
import qualified Vulkan.Setup.Resource.Font as Font

import Game.World

import qualified Render.Pipeline.Line as Line

particle :: Float -> Position -> Velocity -> [Model.Vertex Line.Pos Line.Attrs]
particle fuse (Position (Linear.V2 px py)) (Velocity (Linear.V2 vx vy)) =
  [ Model.Vertex
      { vPosition = Linear.V3 px py 0
      , vAttrs = color
      }
  , Model.Vertex
      { vPosition = Linear.V3 (px + vx * dt) (py + vy * dt) 0
      , vAttrs = color
      }
  ]
  where
    color = Linear.V4 1 (sqrt $ clamp 0 1 fuse) 0 1

step :: SystemW ()
step = do
  incrTime
  stepPosition
  clampPlayer
  clearTargets
  clearBullets
  stepParticles
  handleCollisions
  playerShoot

  Apecs.cmapM_ \TargetRow{..} -> do
    triggerEvery targetRate targetPhase $
      Apecs.newEntity
        ( Target
        , targetPosition
        , targetVelocity
        )

incrTime :: SystemW ()
incrTime =
  Apecs.modify Apecs.global \(Time t) ->
    Time (t + dt)

stepPosition :: SystemW ()
stepPosition =
  Apecs.cmap \(Position pos, Velocity vel) ->
    Position $ pos + vel Linear.^* dt

clampPlayer :: SystemW ()
clampPlayer =
  Apecs.cmap \(Player, Position (Linear.V2 x y)) ->
    Position $ Linear.V2 (clamp xmin xmax x) y

clearTargets :: SystemW ()
clearTargets =
  Apecs.cmap \old@(Target, Position (Linear.V2 x _y), Velocity _vel) ->
    if x < xmin || x > xmax then
      Nothing
    else
      Just old

clearBullets :: SystemW ()
clearBullets =
  Apecs.cmapM \(Bullet, Position (Linear.V2 _x y), Score score) ->
    if y < ymin then do
      let newScore = score - missPenalty
      Apecs.cmapM_ \(Player, scoreLine, entity) -> do
        new <- updateTextLine scoreLine $ "Score: " <> show newScore
        Apecs.set entity new
      pure $ Right
        ( Apecs.Not @(Bullet, Kinetic)
        , Score newScore
        )
    else
      pure $ Left ()

stepParticles :: SystemW ()
stepParticles =
  Apecs.cmap \(Particle time, Velocity vel) ->
    if time <= 0 then
      Right $ Apecs.Not @(Particle, Kinetic)
    else
      Left
        ( Particle (time - dt)
        , Velocity $ vel Linear.^* 0.98
        )

handleCollisions :: SystemW ()
handleCollisions =
  Apecs.cmapM_ \(Target, Position posT, target) ->
    Apecs.cmapM_ \(Bullet, Position posB, bullet) ->
      when (Linear.quadrance (posT - posB) < 50) do
        Apecs.destroy target $ Proxy @(Target, Kinetic)
        Apecs.destroy bullet $ Proxy @(Bullet, Kinetic)
        spawnParticles 15 (Position posT) (-500, 500) (200, -50) 1

        Score score <- Apecs.get Apecs.global
        let newScore = score + hitBonus
        Apecs.set Apecs.global $ Score newScore

        Apecs.cmapM_ \(Player, scoreLine, entity) -> do
          new <- updateTextLine scoreLine $ "Score: " <> show newScore
          Apecs.set entity new

playerShoot :: SystemW ()
playerShoot =
  Apecs.cmapM_ \(Player, Shoot period, Position pos) ->
    triggerEvery period 0 do
      let origin = Position $ pos + V2 0 (-10)
      _bullet <- Apecs.newEntity
        ( origin
        , Bullet
        , Velocity (Linear.V2 0 (-bulletSpeed))
        )
      spawnParticles 7 origin (-80, 80) (-10, -100) 0.5

-- * Helpers

triggerEvery :: Float -> Float -> SystemW a -> SystemW ()
triggerEvery period phase sys = do
  Time t <- Apecs.get Apecs.global

  let
    t' = t + phase
    curr = floor (t' / period) :: Int
    next = floor ((t' + dt) / period)

  when (curr /= next) $
    void sys

spawnParticles
  :: Int
  -> Position
  -> (Float, Float)
  -> (Float, Float)
  -> Float
  -> SystemW ()
spawnParticles n pos dvx dvy maxt =
  replicateM_ n do
    vel <- liftIO $ Linear.V2
      <$> randomRIO dvx
      <*> randomRIO dvy
    fuse <- liftIO $ randomRIO (dt, maxt)
    Apecs.newEntity
      ( Particle fuse
      , pos
      , Velocity vel
      )

dt :: Float
dt = 1/60

xmin :: Float
xmin = -110

xmax :: Float
xmax = 110

ymin :: Float
ymin = -250

ymax :: Float
ymax = 250

hitBonus :: Int
hitBonus = 100

missPenalty :: Int
missPenalty = 40

playerSpeed :: Float
playerSpeed = 170

bulletSpeed :: Float
bulletSpeed = 360

trs :: Linear.V2 Float -> Float -> Float -> Linear.M44 Float
trs t _r s =
  Linear.V4
    (Linear.V4 s 0 0 0)
    (Linear.V4 0 s 0 0)
    (Linear.V4 0 0 s 0)
    (Linear.V4 x y 0 1)
  where
    Linear.V2 x y = t

createTextLine
  :: "font size"        ::: Float
  -> "container width"  ::: Float
  -> "container height" ::: Float
  -> (Font.Origin, Font.Origin)
  -> Text
  -> SystemW TextLine
createTextLine size width height origin msg = do
  tls <- flip Apecs.cfoldM mempty \acc Window{windowContext, windowFont} -> do
    let
      (_scale, pcs) = Font.putLine
        (width, height)
        (0, 0)
        origin
        size
        windowFont
        (Text.unpack msg)

    instances <- Buffer.createCoherent
      windowContext
      Vk.BUFFER_USAGE_VERTEX_BUFFER_BIT
      256
      (Storable.fromList pcs)

    pure $ TextLine
      { textLine      = msg
      , textInstances = instances
      , textSize      = size
      , textWidth     = width
      , textHeight    = height
      , textOrigin    = origin
      } : acc

  case tls of
    [tl] ->
      pure tl
    [] ->
      error "assert: Window component is present"
    _many ->
      error "assert: Window component is unique"

updateTextLine :: TextLine -> [Char] -> SystemW TextLine
updateTextLine tl@TextLine{..} msg = do
  changes <- flip Apecs.cfoldM mempty \acc Window{windowContext, windowFont} -> do
    let
      (_scale, pcs) = Font.putLine
        (textWidth, textHeight)
        (0, 0)
        textOrigin
        textSize
        windowFont
        msg
    new <- Buffer.updateCoherentResize_ windowContext (Storable.fromList pcs) textInstances
    pure $ tl
      { textLine      = Text.pack msg
      , textInstances = new
      }
      : acc
  case changes of
    [one] ->
      pure one
    [] ->
      error "assert: windowContext is present"
    _many ->
      error "assert: windowContext is unique"
