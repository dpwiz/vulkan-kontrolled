module Game.Events.Kontrol where

import App.Prelude

import Linear (V2(..))

import qualified Apecs
import qualified Linear
import qualified Sound.ControlHold.Client as Client (Handler)
import qualified Sound.ControlHold.Client.NanoKontrol2 as Nk

import Game.World

import qualified Game.Events.System as System

handler :: World -> Client.Handler
handler world = Nk.mkHandler_ $
  Apecs.runWith world . \case
    Nk.Group 0 group ->
      case group of
        Nk.Slider alpha ->
          Apecs.cmap \Player ->
            if alpha == 0 then
              Left $ Apecs.Not @Shoot
            else
              Right $ Shoot $ lerp alpha 1 (1/60)
        _ignore ->
          pure ()

    Nk.Group 1 group ->
      Apecs.cmapM_ \(row, entity) ->
        when (targetRow row == 1) $
          case group of
            Nk.Knob alpha -> do
              let
                vel = Linear.angle (lerp alpha 0 $ pi/2) Linear.^* 80
              Apecs.set entity row
                { targetVelocity = Velocity vel
                }

            Nk.Slider alpha ->
              Apecs.set entity row
                { targetRate = lerp alpha 2 (3/32)
                }

            _ignore ->
              pure ()

    Nk.Group 2 group ->
      Apecs.cmapM_ \(row, entity) ->
        when (targetRow row == 2) $
          case group of
            Nk.Knob alpha -> do
              let
                vel = Linear.angle (lerp alpha (pi/2) (pi)) Linear.^* 80
              Apecs.set entity row
                { targetVelocity = Velocity vel
                }

            Nk.Slider alpha ->
              Apecs.set entity row
                { targetRate = lerp alpha 2 (3/32)
                }

            _ignore ->
              pure ()


    Nk.Group 7 group ->
      case group of
        Nk.Knob alpha ->
          Apecs.cmapM \(Player, Position (V2 _oldX posY)) ->
            pure . Position $ V2 (lerp alpha System.xmin System.xmax) posY

        Nk.Slider alpha ->
          Apecs.cmapM \(Player, Position (V2 posX _oldY)) ->
            pure . Position $ V2 posX (lerp alpha System.ymax System.ymin)
        _ignore ->
          pure ()

    _ignore ->
      pure ()
