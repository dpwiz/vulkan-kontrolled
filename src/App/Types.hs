module App.Types where

import RIO

import RIO.Process (HasProcessContext(..), ProcessContext)
import Vulkan.Setup.Window.SDL (SDLContext)

import qualified Vulkan.Setup.Context as Context
import qualified Vulkan.Setup.Resource.Font as Font

-- | Command line arguments
data Options = Options
  { optionsVerbose :: Bool
  }

data App = App
  { appLogFunc        :: LogFunc
  , appProcessContext :: ProcessContext
  , appOptions        :: Options

  , appFont           :: Font.Container
  , appContext        :: AppContext
  }

type AppContext = SDLContext

instance HasLogFunc App where
  logFuncL =
    lens appLogFunc (\x y -> x { appLogFunc = y })

instance HasProcessContext App where
  processContextL =
    lens appProcessContext (\x y -> x { appProcessContext = y })

instance Context.HasVkPhysical App where
  {-# INLINE getVkPhysical #-}
  getVkPhysical = Context.getVkPhysical . appContext

instance Context.HasVkLogical App where
  {-# INLINE getVkLogical #-}
  getVkLogical = Context.getVkLogical . appContext

instance Context.HasVkAllocator App where
  {-# INLINE getVkAllocator #-}
  getVkAllocator = Context.getVkAllocator . appContext
